use anyhow::Result;
use lava_api::device::Health;
use log::{debug, warn};
use mattermost_rs::{message::MessageBuilder, Mattermost};
use std::collections::HashMap;
use std::sync::Arc;
use std::time::Duration;
use tokio::time::sleep;

use crate::devicecache::DeviceCache;

#[derive(Clone, Debug)]
struct MonitorDevice {
    reported: Health,
}

pub struct Monitor {
    devicecache: Arc<DeviceCache>,
    mattermost: Mattermost,
    devices: HashMap<String, MonitorDevice>,
}

#[derive(Clone, Debug)]
enum DeviceChange {
    New(String, Health),
    StateChange(String, Health),
    Removed(String),
}

impl Monitor {
    async fn check(&self, max_age: Duration) -> Result<Vec<DeviceChange>> {
        let devices = self.devicecache.devices_since(max_age).await?;
        let mut changes: Vec<DeviceChange> = Vec::new();

        for (name, device) in devices.iter() {
            if let Some(entry) = self.devices.get(name) {
                if entry.reported != device.derived_health {
                    changes.push(DeviceChange::StateChange(
                        device.hostname.clone(),
                        device.derived_health,
                    ));
                }
            } else {
                changes.push(DeviceChange::New(
                    device.hostname.clone(),
                    device.derived_health,
                ));
            }
        }

        for name in self.devices.keys() {
            if !devices.contains_key(name) {
                changes.push(DeviceChange::Removed(name.clone()));
            }
        }

        Ok(changes)
    }

    fn update(&mut self, changes: Vec<DeviceChange>) {
        for change in changes {
            match change {
                DeviceChange::New(d, reported) => {
                    self.devices.insert(d, MonitorDevice { reported });
                }
                DeviceChange::StateChange(d, reported) => {
                    self.devices.insert(d, MonitorDevice { reported });
                }
                DeviceChange::Removed(d) => {
                    self.devices.remove(&d);
                }
            }
        }
    }

    async fn report(&self, changes: &[DeviceChange]) -> Result<()> {
        let mut text = "Device changes:\n".to_string();
        for change in changes {
            let t = match change {
                DeviceChange::New(d, _) => format!("* New devices: {}", d),
                DeviceChange::Removed(d) => format!("* Device removed: {}", d),
                DeviceChange::StateChange(d, state) => {
                    format!("* Device {} went to state {:?}", d, state)
                }
            };
            text.push_str(&t);
            text.push('\n');
        }

        debug!("Sending mattermost message: {:?}", text);

        let msg = MessageBuilder::default().text(&text).build()?;
        self.mattermost.send(msg).await?;
        Ok(())
    }

    pub async fn monitor(
        devicecache: Arc<DeviceCache>,
        mattermost: Mattermost,
        interval: Duration,
    ) -> ! {
        let d = loop {
            match devicecache.devices_since(interval).await {
                Ok(d) => break d,
                Err(e) => warn!("Failed to get initial devices from lava: {}", e),
            }

            sleep(interval).await;
        };

        let devices = d.iter().fold(HashMap::new(), |mut acc, (k, d)| {
            acc.insert(
                k.clone(),
                MonitorDevice {
                    reported: d.derived_health,
                },
            );
            acc
        });

        let mut monitor = Monitor {
            devicecache,
            mattermost,
            devices,
        };

        loop {
            sleep(interval).await;
            debug!("Checking for changes");

            match monitor.check(interval / 4).await {
                Ok(changes) if changes.is_empty() => {
                    debug!("Nothing changed");
                }
                Ok(changes) => {
                    debug!("Reporting changes to mattermost");
                    if let Err(e) = monitor.report(&changes).await {
                        warn!("Failed to report to mattermost: {}", e);
                    } else {
                        monitor.update(changes);
                    }
                }
                Err(e) => warn!("Failed to check devices: {}", e),
            }
        }
    }
}
