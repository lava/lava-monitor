use anyhow::Result;
use chrono::{DateTime, Utc};
use futures::join;
use futures::stream::TryStreamExt;
use log::debug;
use tokio::sync::Mutex;

use std::collections::{btree_map, BTreeMap, BTreeSet};
use std::sync::{Arc, Mutex as StdMutex, RwLock, RwLockReadGuard};
use std::time::Duration;

use lava_api::{job::Job, job::Ordering, job::State, Lava};

type Map = BTreeMap<i64, Job>;

/// The creation parameters for a [JobCache]
#[derive(Debug, Clone)]
pub struct JobCacheParams {
    /// The [Lava] instance to use for queries.
    pub lava: Arc<Lava>,
    /// The maximum age of a finished job in the cache. All unfinished
    /// jobs are always present in the cache.
    pub age_limit: Duration,
    /// `time_safety_margin` is the amount of time to overlap queries
    /// by.
    ///
    /// It is required because we request jobs that completed since
    /// our last query, but we don't know the last timestamp that was
    /// present in the Lava database at the moment of that last
    /// query. Optimistically, it could be the same as our local time,
    /// but practical experience shows it is not. This leads to jobs
    /// that slip between the query windows, and end up stuck in the
    /// cache in a non-final state. The safety margin indicates the
    /// maximum delta between our local time upon launching a query
    /// and the latest timestamp that could be present in the job data
    /// returned.
    ///
    /// Setting `time_safety_margin` to a larger value reduces the
    /// risk of missed transitions (and thus cache inflation and some
    /// invalid metrics) but increases the cost of repeated queries.
    /// A `time_safety_margin` of zero is unsafe, but a
    /// `time_safety_margin` of an hour is likely wasteful.
    pub time_safety_margin: Duration,
}

/// A cache of Lava [Job]s that can be iterated over in job id order.
/// The cache contains all jobs up to some age, as well as all active
/// jobs (not in [State::Finished]). Forcing the inclusion of all
/// active jobs means metrics based on job state are always as
/// accurate as possible, rather than being subject to horizon
/// effects. Garbage collection of old, finished jobs must be
/// triggered manually, to ensure transitions are not missed.
#[derive(Debug)]
pub struct JobCache {
    lava: Arc<Lava>,
    /// The actual job data that is buffered, in id order.
    jobs: RwLock<Map>,
    /// The maximum age of a finished job in the cache.
    age_limit: Duration,
    /// The maximum amount of skew expected for a job's associated
    /// timestamps versus local time
    time_safety_margin: Duration,
    /// The time the last update started. The lock should be held for
    /// the full duration of the update process to ensure proper
    /// updates.
    last_update: Mutex<DateTime<Utc>>,
    /// Set of jobs that have been expired
    expired: StdMutex<BTreeSet<i64>>,
}

impl JobCache {
    pub async fn new(params: JobCacheParams) -> Result<Self> {
        let cutoff = Utc::now() - chrono::Duration::from_std(params.age_limit).unwrap();
        let last_update = Mutex::new(Utc::now());
        let jobs = Self::populate_initial_jobs(&params.lava, cutoff).await?;
        let jobs = RwLock::new(jobs);
        let expired = StdMutex::new(BTreeSet::new());

        Ok(JobCache {
            lava: params.lava,
            jobs,
            age_limit: params.age_limit,
            time_safety_margin: params.time_safety_margin,
            last_update,
            expired,
        })
    }

    async fn populate_initial_jobs(lava: &Lava, cutoff: DateTime<Utc>) -> Result<Map> {
        debug!("Initial Populating of job cache started");
        let mut jobs = Map::new();
        // These two queries can't run in parallel, because that
        // removes any ordering between the two data sources, which
        // means we don't know which branch to select as truthfully
        // representing the current state.
        let mut lj = lava
            .jobs()
            .limit(2000)
            .ordering(Ordering::Id, false)
            .submitted_after(cutoff)
            .query();

        while let Some(job) = lj.try_next().await? {
            jobs.insert(job.id, job);
        }

        // The previous query only returns jobs submitted after some
        // cutoff point (we don't want to stream the entire database
        // over HTTP every time the monitor starts). This next query
        // retrieves all unfinished jobs, some of which might have
        // been missing from the previous result set because of the
        // age limit. This ensures that the cache begins from a
        // consistent state in which all all unfinished jobs are
        // available.
        let mut lj = lava
            .jobs()
            .state_not(State::Finished)
            .ordering(Ordering::Id, false)
            // This limit has to be set high enough that paging does not occur
            // otherwise jobs can be lost at page boundaries, because the result
            // set does not strictly increase in size over time. The only reason
            // any limit is specified is lava will apply a default (small) limit
            // when this is omitted.
            .limit(2000)
            .query();

        while let Some(job) = lj.try_next().await? {
            jobs.insert(job.id, job);
        }

        debug!("Initial Populating of job cache finished");
        Ok(jobs)
    }

    fn age_cutoff_now(&self) -> DateTime<Utc> {
        Utc::now() - chrono::Duration::from_std(self.age_limit).unwrap()
    }

    fn cleanup_expired(&self) {
        /* Lock the jobs first to maintain the same order as JobCacheReader::cleanup */
        let mut jobs = self.jobs.write().unwrap();
        let expired = self.expired.lock().unwrap();

        for id in expired.iter() {
            jobs.remove(id);
        }
    }

    /// Refresh the cache. This needs to be called regularly to keep the buffer up to date.
    pub async fn update(&self) -> Result<()> {
        self.cleanup_expired();

        // Take the last_update lock before doing the update and keep it over it; this ensures
        // updates can only happen in serial
        let mut last_update = self.last_update.lock().await;
        let update_start = Utc::now();

        self.update_delta(&last_update).await?;
        *last_update = update_start;
        Ok(())
    }

    async fn update_delta(&self, last_update: &DateTime<Utc>) -> Result<()> {
        debug!("Updating job cache started");
        let update_active = async {
            let mut jobs = Vec::new();
            let mut lj = self
                .lava
                .jobs()
                .limit(2000)
                .state_not(State::Finished)
                .query();

            while let Some(job) = lj.try_next().await? {
                jobs.push(job);
            }
            Ok::<_, anyhow::Error>(jobs)
        };

        let cutoff = *last_update - chrono::Duration::from_std(self.time_safety_margin).unwrap();

        let update_recently_finished = async {
            let mut jobs = Vec::new();
            let mut lj = self.lava.jobs().limit(2000).ended_after(cutoff).query();
            while let Some(job) = lj.try_next().await? {
                jobs.push(job);
            }
            Ok::<_, anyhow::Error>(jobs)
        };

        let (jobs_active, jobs_finished) = join!(update_active, update_recently_finished);

        debug!("Updating job cache data retrieval finished");
        let mut jobs = self.jobs.write().unwrap();
        for job in jobs_active? {
            jobs.insert(job.id, job);
        }
        for job in jobs_finished? {
            jobs.insert(job.id, job);
        }

        debug!("Updating job cache finished");
        Ok(())
    }

    /// Returns a JobCacheReader object which can provide an iterator. The reader will keep a lock
    /// on the internal data structure; So it should be dropped before calling any other functions
    /// of this type.
    pub fn reader(&self) -> JobCacheReader {
        JobCacheReader {
            read: self.jobs.read().unwrap(),
            cache: self,
        }
    }
}

pub struct JobCacheReader<'a> {
    read: RwLockReadGuard<'a, Map>,
    cache: &'a JobCache,
}

type JobCacheIter<'a> = btree_map::Values<'a, i64, Job>;
impl JobCacheReader<'_> {
    /// Return an iterator over the jobs in the JobCache. Note that jobs
    /// will be returned in id order, without duplicates, but that the
    /// ids may not be dense: there may be a gap between two jobs
    /// where a job is missing from the buffer.
    pub fn iter(&self) -> JobCacheIter {
        self.read.values()
    }

    /// cleanup all finished jobs that are older than the age limit for cached data.
    pub fn cleanup(&self) {
        let target = self.cache.age_cutoff_now();
        let mut expired = self.cache.expired.lock().unwrap();
        for j in self
            .iter()
            .take_while(|j| j.submit_time <= target)
            .filter(|j| j.state == State::Finished)
        {
            expired.insert(j.id);
        }
    }
}
