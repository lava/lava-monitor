use anyhow::{Context, Result};
use lava_api::job;
use prometheus::{opts, register_gauge_vec, Encoder, GaugeVec, TextEncoder};
use std::collections::HashSet;
use std::sync::Arc;
use warp::Filter;

use crate::devicecache::DeviceCache;
use crate::jobcache::JobCache;

#[derive(Clone)]
pub struct Metrics {
    devicecache: Arc<DeviceCache>,
    jobcache: Arc<JobCache>,
    devices: GaugeVec,
    devices_active: GaugeVec,
    tags: GaugeVec,
    workers: GaugeVec,
    queue_size: GaugeVec,
    running_job_seconds: GaugeVec,
}

impl warp::reject::Reject for AnyhowRejection {}
#[derive(Debug)]
struct AnyhowRejection {
    error: anyhow::Error,
}

impl Metrics {
    pub fn new(devicecache: Arc<DeviceCache>, jobcache: Arc<JobCache>) -> Result<Metrics> {
        let opts = opts!("lava_device", "Status of Lava Devices Under Test");
        let devices = register_gauge_vec!(opts, &["health", "device", "device_type"])
            .context("Failed to register board gauges")?;

        let opts = opts!("lava_device_active", "Activity of Lava Devices Under Test");
        let devices_active = register_gauge_vec!(opts, &["device", "device_type"])
            .context("Failed to register activity gauges")?;

        let opts = opts!("lava_worker", "Lava device to worker mapping");
        let workers = register_gauge_vec!(opts, &["worker", "device", "location"])
            .context("Failed to register worker gauges")?;

        let opts = opts!("lava_tag", "Lava device to tags mapping");
        let tags = register_gauge_vec!(opts, &["tag", "device"])
            .context("Failed to register tag gauges")?;

        let opts = opts!("lava_queue_size", "Lava queue size");
        let queue_size = register_gauge_vec!(opts, &["device_type", "submitter", "priority"])
            .context("Failed to register device queue gauges")?;

        let opts = opts!(
            "lava_running_job_seconds",
            "Time in seconds of the current running jobs"
        );
        let running_job_seconds = register_gauge_vec!(opts, &["device_type", "actual_device"])
            .context("Failed to register running jobs gauges")?;

        Ok(Metrics {
            devicecache,
            jobcache,
            devices,
            devices_active,
            tags,
            workers,
            queue_size,
            running_job_seconds,
        })
    }

    async fn update_gauges(&self) -> Result<()> {
        fn worker_to_location(worker: &str) -> &str {
            let parts: Vec<&str> = worker.split('-').collect();
            if parts.len() == 4 {
                return parts[2];
            }

            if worker.contains("cbg") {
                return "cbg";
            }

            "unknown"
        }

        let mut active_devices = HashSet::new();

        let devices = self.devicecache.devices().await?;
        let jobs = self.jobcache.reader();

        self.running_job_seconds.reset();
        for j in jobs.iter().filter(|j| j.state == job::State::Running) {
            let start_time = j.start_time.unwrap();
            let actual_device = j
                .actual_device
                .clone()
                .unwrap_or_else(|| "unknown_device".to_string());
            self.running_job_seconds
                .with_label_values(&[&j.requested_device_type, &actual_device])
                .set((chrono::Utc::now() - start_time).num_seconds() as f64);
            active_devices.insert(actual_device);
        }

        self.devices.reset();
        self.devices_active.reset();
        self.workers.reset();

        for (hostname, d) in devices.iter() {
            use lava_api::device::Health::*;
            let state = match d.health {
                Unknown => "unknown",
                Maintenance => "maintenance",
                Good => "good",
                Bad => "bad",
                Looping => "looping",
                Retired => continue,
            };

            let active = active_devices.contains(hostname);

            self.devices
                .with_label_values(&[state, hostname, &d.device_type])
                .set(1f64);
            self.devices_active
                .with_label_values(&[hostname, &d.device_type])
                .set(if active { 1f64 } else { 0f64 });
            let location = worker_to_location(&d.worker);
            self.workers
                .with_label_values(&[&d.worker, hostname, location])
                .set(1f64);
            for t in d.tags.iter() {
                self.tags.with_label_values(&[&t.name, hostname]).set(1f64);
            }
        }

        self.queue_size.reset();

        for j in jobs.iter().filter(|j| j.state == job::State::Submitted) {
            self.queue_size
                .with_label_values(&[
                    &j.requested_device_type,
                    &j.submitter,
                    &j.priority.to_string(),
                ])
                .inc();
        }

        // We mark the job cache for cleanup here, as we know its current contents have been recorded in
        // the metrics.
        jobs.cleanup();

        Ok(())
    }

    pub async fn listen(self) {
        let metrics = warp::path!("metrics").and_then(move || {
            let s = self.clone();

            async move {
                s.update_gauges()
                    .await
                    .map_err(|error| warp::reject::custom(AnyhowRejection { error }))?;

                let encoder = TextEncoder::new();
                let families = prometheus::gather();
                let mut buffer = Vec::new();
                encoder
                    .encode(&families, &mut buffer)
                    .expect("Encoding failed");

                if false {
                    Err(warp::reject::not_found())
                } else {
                    Ok(buffer)
                }
            }
        });

        warp::serve(metrics).run(([0, 0, 0, 0], 8884)).await;
    }
}
