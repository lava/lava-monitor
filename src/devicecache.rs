use anyhow::Result;
use futures::stream::TryStreamExt;
use lava_api::{device::Health, tag::Tag, Lava};
use log::debug;
use std::collections::HashMap;
use std::sync::Arc;
use std::time::{Duration, Instant};
use tokio::sync::RwLock;

#[derive(Debug, Clone)]
pub struct Device {
    pub hostname: String,
    pub worker: String,
    pub device_type: String,
    // Last health as reported by the lava api
    pub health: Health,
    // Derived health
    pub derived_health: Health,
    pub tags: Vec<Tag>,
    visited: bool,
}

#[derive(Debug)]
struct CacheInner {
    update: Option<Instant>,
    devices: Arc<HashMap<String, Device>>,
}

#[derive(Debug)]
pub struct DeviceCache {
    lava: Arc<Lava>,
    inner: RwLock<CacheInner>,
}

impl DeviceCache {
    pub async fn new(lava: Arc<Lava>) -> Self {
        let inner = RwLock::new(CacheInner {
            update: None,
            devices: Arc::new(HashMap::new()),
        });
        Self { lava, inner }
    }

    pub async fn devices_since(&self, max_age: Duration) -> Result<Arc<HashMap<String, Device>>> {
        {
            let inner = self.inner.read().await;
            if let Some(earlier) = inner.update {
                let now = Instant::now();
                if now.duration_since(earlier) < max_age {
                    debug!("Returning cached content");
                    return Ok(inner.devices.clone());
                }
            }
        }
        debug!("Refreshing the device cache");
        self.devices().await
    }

    pub async fn devices(&self) -> Result<Arc<HashMap<String, Device>>> {
        let mut inner = self.inner.write().await;
        let now = Instant::now();

        // Copy on write if needed
        let devices = Arc::make_mut(&mut inner.devices);

        for (_, v) in devices.iter_mut() {
            v.visited = false
        }

        let mut ld = self.lava.devices();
        while let Some(d) = ld.try_next().await? {
            use Health::*;
            if d.health == Retired {
                continue;
            }

            if let Some(entry) = devices.get_mut(&d.hostname) {
                entry.tags = d.tags;
                entry.worker = d.worker_host;
                entry.visited = true;
                entry.health = d.health;

                // Only update the derived health on "useful" transitions;
                // Unknown is never very helpful to report and going bad or
                // looping after maintaince is just an extension of
                // maintenance
                let update = match (entry.derived_health, d.health) {
                    (o, n) if o == n => false,
                    (_, Unknown) => false,
                    (Maintenance, Bad) => false,
                    (Maintenance, Looping) => false,
                    _ => true,
                };

                if update {
                    entry.derived_health = d.health;
                }
            } else {
                devices.insert(
                    d.hostname.clone(),
                    Device {
                        hostname: d.hostname.clone(),
                        worker: d.worker_host.clone(),
                        device_type: d.device_type.clone(),
                        visited: true,
                        health: d.health,
                        derived_health: d.health,
                        tags: d.tags,
                    },
                );
            }
        }

        devices.retain(|_, v| v.visited);
        inner.update = Some(now);
        Ok(inner.devices.clone())
    }
}
